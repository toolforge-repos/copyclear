#!/usr/bin/env python3
# coding: utf-8

# In[2]:


import pywikibot
import csv
import re
import requests
import urllib.parse
import time
import re
import datetime

from pywikibot import pagegenerators as pg


with open('DACS-addStatus.rq', 'r') as query_file:
    QUERY = query_file.read()

wikidata_site = pywikibot.Site("wikidata", "wikidata")
generator = pg.WikidataSPARQLPageGenerator(QUERY, site=wikidata_site)

for item in generator:
    
    site = pywikibot.Site("wikidata", "wikidata")
    repo = site.data_repository()
    item_dict = item.get() #Get the item dictionary
    clm_dict = item_dict["claims"] # Get the claim dictionary
    clm_list = clm_dict["P4663"] #DACS
    for clm in clm_list:
        clm_trgt = clm.getTarget()
        print(clm_trgt)
        url = "https://www.dacs.org.uk/licensing-works/artist-search/artist-details?ArtistId=" + clm_trgt
        print (url)
        r = requests.get(url)
        #time.sleep(2)
        text = r.text 
        text = text.replace('\n', ' ').replace('\r', '')
        pattern = 'DACS (does not represent|represents) this artist for Copyright Licensing'
        #DACS does not represent this artist for Copyright Licensing
        a = re.search(pattern, text)
        rep = ""
        code = clm_trgt
        if a:
            rep = a.group(1)
            print (rep)
            claim = pywikibot.Claim(repo, u'P6275')
            if (rep == "does not represent"):
                target = pywikibot.ItemPage(repo, u"Q71521142")
            if (rep == "represents"):
                target = pywikibot.ItemPage(repo, u"Q71528227")
            claim.setTarget(target)
            statedin = pywikibot.Claim(repo, u'P248')
            itis = pywikibot.ItemPage(repo, "Q71534274")
            statedin.setTarget(itis)
            
            dacscode = pywikibot.Claim(repo, u'P4663')
            dacscode.setTarget(clm_trgt)

            refdate = pywikibot.Claim(repo, u'P813')
            today = datetime.datetime.today()
            date = pywikibot.WbTime(year=today.year, month=today.month, day=today.day)
            refdate.setTarget(date)
#            claim.addSources([statedin, refdate], summary=u'Adding sources.')
            claim.addSources([statedin, refdate, dacscode], summary=u'Adding sources.')
            item.addClaim(claim, summary=u'Add DACS status')
#            with open("DACSstatus.csv", "a", newline="", encoding='utf-8') as csvf:
#                fields = ["qid", "code", "rep"]
#                writer = csv.DictWriter(csvf, fieldnames=fields)
                #writer.writeheader()
#                writer.writerow({"qid":item, "code":code, "rep":rep})
#            break

#            website = x.group(1)
#            if website.find("wikipedia") == -1:
#                if website.find(" ") == -1:
#                    stringclaim = pywikibot.Claim(repo, u'P856')
#                    stringclaim.setTarget(website)
#                    item.addClaim(stringclaim, summary=u'Adding URL from RKD')
                
 
 


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




